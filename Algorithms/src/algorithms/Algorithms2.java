/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package algorithms;

import java.lang.reflect.Array;
import java.util.Arrays;

/**
 *
 * @author 8888
 */
public class Algorithms2 {

  /**
   * @param args the command line arguments
   */
  public static void main(String[] args) {
    // TODO code application logic here
    //int kervin = intFactorial(6);    
    //System.out.println(kervin);
    //int kervin = intRecursive(6);
    //System.out.println(kervin);
    //System.out.println(isPalindrome("racecar"));
    //System.out.println(power(2, 8));
    int[] array = {14, 7, 3, 12, 9, 11, 6, 2};
    mergeSort(array, 0, array.length - 1);
    System.out.println(Arrays.toString(array)); 
  }

  public static int intFactorial (int facto) {
    int kevin = 0;
    int mule = 1;
    for(int i = 1; i <= facto; i++){
      mule = i * mule;
      //System.out.print(mule);
    }
    return mule;
  }

  public static int intRecursive (int factor) {
    if (factor<=0) {
      return 1;
    }
    else {
      factor = factor * intRecursive(factor-1);
    }
    return factor;
  }

  public static boolean isPalindrome (String pali) {
    String straw = "";
    int x = pali.length();
    if (x <= 1) {
      return true;
    }
    else if (firstCharacter(pali) != lastCharacter(pali)) {
      return false;
    }
    else {
      return isPalindrome(pali.substring(1, x - 1));
    }


  }

  public static char firstCharacter (String pali) {
    return pali.charAt(0);                                                              
  }

  public static char lastCharacter (String pali) {
    return pali.charAt(pali.length() - 1);                                                              
  }

  public static String middleCharacters (String pali) {
    String s = "";
    for (int i = 1; i < (pali.length() - 1); i++) {
      s = s  + pali.charAt(i);
    }
    return s;    
  }

  public static double power (double x, double n) {
    return n == 0? 1 : power(x, n - 1)*x;    
  }

  public static void mergeSort(int array[], int p, int q) {
    if ((q - p) <= 1) {
     if (array[q] < array[p]) {
     int temp = array[q];
     array[q] = array[p];
     array[p] = temp;
     }
    }
    
    else {
      mergeSort(array, p, p + (q - p)/2);
      mergeSort(array, p + (q - p)/2, q);
      merge(array, p , p + (q - p)/2, q);
    }
  }

  public static void merge(int[] array, int start, int mid, int end) {
    /*
     * Method:
     * start-mid is the first array
     * mid + 1 to end is second array
     * )
     * Take the two arrays, and look at the first value.  
     * Take the smaller, and remove it, and add it to your final array
     * Repeat until done
     */
    //System.out.println(Arrays.toString(array));
    int p = start; //p is counter for first array
    int q = mid; //q is counter for second
    int[] newArr = new int[end - start + 1];
    for(int i = 0; i < newArr.length; i++) {
      if(p == mid) { //if the first arry has been filled
	newArr[i] = array[q++]; //just dump the next data from the other array and continue
	continue;
      } 
      else if(q == end) {
	newArr[i] = array[p++];
	continue;
      }
     /*
      * sets the value to the lower of the two arrays and increments the counters
      */
      newArr[i] = (array[p] < array[q])? array[p++] : array[q++];
    }
    
    for (int i = 0; i < newArr.length; i++) {
    array[start + i] = newArr[i];    
    }
  }
}
